function logout() {
  if (window.sessionStorage.getItem('uname')) {
    window.sessionStorage.removeItem('uname');
  }
  window.location = 'https://secure-cc34.com';
}
$(document).ready(function () {
  var uname = window.sessionStorage.getItem('uname');
  if (!uname) {
    $('body').hide();
    window.location = 'https://secure-cc34.com';
  }
  $('.log-out').click(() => {
    logout();
  });
});

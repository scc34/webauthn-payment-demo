async function register() {
  if (!window.fetch || !navigator.credentials || !navigator.credentials.create) {
    alert('Browser not supported.');
    return;
  }

  // get registration options object
  const response1 = await window.fetch('/api/server.php?route=getRegOpts&un=' + $('#uname').val(), { method: 'GET', cache: 'no-cache' });
  let regRequestData = await response1.json();

  console.log('pubKeyRegOpts', regRequestData);

  if (regRequestData.success === false) {
    $('#err-txt').show();
    return;
  }

  //user  reviver of JSON.parse to convert all values from binary base 64 strings to ArrayBuffer
  //since that is what navigator.credentials.create accepts
  regRequestData = JSON.parse(JSON.stringify(regRequestData), reviver);

  // create credential. navigator.credentials.create does the CTAP2(client to authenticator protocol)
  const cred = await navigator.credentials.create(regRequestData);

  console.log('regCred', cred);

  // convert to base64
  const AuthenticatorAttestationResponse = JSON.stringify({
    clientDataJSON: cred.response.clientDataJSON ? arrayBufferToBase64(cred.response.clientDataJSON) : null,
    attestationObject: cred.response.attestationObject ? arrayBufferToBase64(cred.response.attestationObject) : null,
  });

  const response2 = await window.fetch('/api/server.php?route=processReg&un=' + $('#uname').val(), {
    method: 'POST',
    body: AuthenticatorAttestationResponse,
    cache: 'no-cache',
  });

  // convert to JSON
  const regResponse = await response2.json();
  console.log('regResponse', regResponse);

  if (regResponse.success) {
    let user_name = $('#uname').val();
    window.sessionStorage.setItem('uname', user_name);
    showSuccessAlert();
  } else {
    console.log(regResponse.msg);
  }
}

async function authenticate() {
  if (!window.fetch || !navigator.credentials || !navigator.credentials.create) {
    alert('Browser not supported.');
    return;
  }

  let uname;
  if ($('#uname').val()) {
    uname = $('#uname').val();
  } else {
    uname = window.sessionStorage.getItem('uname');
  }
  const response1 = await window.fetch('/api/server.php?route=getAuthOpts&un=' + uname, { method: 'GET', cache: 'no-cache' });
  let authRequestData = await response1.json();
  if (authRequestData.success === false) {
    $('#err-txt').show();
    return;
  }

  console.log('pubKeyAuthOpts', authRequestData);

  //user  reviver of JSON.parse to convert all values from binary base 64 strings to ArrayBuffer
  //since that is what navigator.credentials.get accepts
  authRequestData = JSON.parse(JSON.stringify(authRequestData), reviver);

  const cred = await navigator.credentials.get(authRequestData);
  console.log('authCred', cred);

  //convert to base64
  const AuthenticatorAttestationResponse = JSON.stringify({
    id: cred.rawId ? arrayBufferToBase64(cred.rawId) : null,
    clientDataJSON: cred.response.clientDataJSON ? arrayBufferToBase64(cred.response.clientDataJSON) : null,
    authenticatorData: cred.response.authenticatorData ? arrayBufferToBase64(cred.response.authenticatorData) : null,
    signature: cred.response.signature ? arrayBufferToBase64(cred.response.signature) : null,
  });

  const response2 = await window.fetch('/api/server.php?route=processAuth&un=' + uname, {
    method: 'POST',
    body: AuthenticatorAttestationResponse,
    cache: 'no-cache',
  });
  const authResponse = await response2.json();
  console.log('authResponse', authResponse);

  if (authResponse.success) {
    if ($('#uname').val()) {
      let user_name = $('#uname').val();

      window.sessionStorage.setItem('uname', user_name);
      showSuccessAlert();
    } else {
      $('#headertochange').removeClass('bg-dark').addClass('bg-success');
      var verify_msg = `<i class="far fa-check-circle "></i> Payment submission was successful`;
      $('#ver_msg').html(verify_msg);
      $('.configurable_buttons').empty();
      $('.configurable_buttons').html(`<button class='btn btn-md btn-secondary' id = 'close' >Close</button>`);
      $('#close').click(() => {
        window.location = 'https://secure-cc34.com/gotodemo.html';
      });
    }
  } else {
    console.log(authResponse.msg);
  }
}

function reviver(key, value) {
  let prefix = '=?BINARY?B?';
  let suffix = '?=';
  if (typeof value === 'string') {
    let str = value;
    if (str.substring(0, prefix.length) === prefix && str.substring(str.length - suffix.length) === suffix) {
      str = str.substring(prefix.length, str.length - suffix.length);

      let binary_string = window.atob(str);
      let len = binary_string.length;
      let bytes = new Uint8Array(len);
      for (let i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
      }
      return bytes.buffer;
    }
    return value;
  }
  return value;
}

//converts ArrayBuffer to base64
function arrayBufferToBase64(buffer) {
  let binary = '';
  let bytes = new Uint8Array(buffer);
  let len = bytes.byteLength;
  for (let i = 0; i < len; i++) {
    binary += String.fromCharCode(bytes[i]);
  }
  return window.btoa(binary);
}

//success alert using sweet alert
function showSuccessAlert() {
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn-socure',
      denyButton: 'btn-socure',
    },
  });

  let title;
  let redirectBtnText;
  if (reg) {
    //if (caller === 'reg') {
    title = 'Registration Success!\n\n Choose next demo below';
    redirectBtnText = 'Sign in';
  } else {
    title = 'Sign in Success!\n\n Choose next demo below';
    redirectBtnText = 'New Registration';
  }
  swalWithBootstrapButtons
    .fire({
      icon: 'success',
      title: title,
      showDenyButton: true,
      confirmButtonText: `3DS2.0 Auth`,
      denyButtonText: redirectBtnText,
    })
    .then((result) => {
      if (result.isConfirmed) {
        window.location = 'https://secure-cc34.com/pay.html';
      } else if (result.isDenied) {
        toggleRegAuth();
      }
    });
}

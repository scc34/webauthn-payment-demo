function declinePymnt() {
  $('#headertochange').removeClass('bg-dark').addClass('bg-secondary');
  var verify_msg = `<i class="far fa-times-circle"></i> Payment rejected`;
  $('#ver_msg').html(verify_msg);
  $('.configurable_buttons').empty();
  $('.configurable_buttons').html(`<button class='btn btn-md btn-secondary' id='close'>
                    Close</button>`);
  $('#close').click(() => {
    window.location = 'https://secure-cc34.com/gotodemo.html';
  });
}

function logout() {
  if (window.sessionStorage.getItem('uname')) {
    window.sessionStorage.removeItem('uname');
  }
  window.location = 'https://secure-cc34.com';
}

$(document).ready(function () {
  if (location.protocol !== 'https:' && location.host !== 'localhost') {
    location.href = location.href.replace('http', 'https');
  }

  today = new Date().toLocaleDateString();
  $('#date_now').text(today);

  var localTime = new Date().toLocaleTimeString();
  $('#time_now').text(localTime);
  uname = window.sessionStorage.getItem('uname');
  if (!uname) {
    $('body').hide();
    window.location = 'https://secure-cc34.com';
  }
  var verify_msg = `${uname}, please verify payment for this order`;
  $('#ver_msg').text(verify_msg);
  $('#pay').click(() => {
    authenticate();
  });
  $('#decline').click(() => {
    declinePymnt();
  });
  $('.log-out').click(() => {
    logout();
  });
});

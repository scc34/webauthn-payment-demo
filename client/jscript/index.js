let reg = true;
$(document).ready(function () {
  $('#err-txt').hide();
  if (location.protocol !== 'https:' && location.host !== 'localhost') {
    location.href = location.href.replace('http', 'https');
  }
  $('#uname').keyup(() => {
    $('#err-txt').hide();
  });
  $('.btn').click(() => {
    $('#uname').val($('#uname').val().trim());
    if (!$('#uname').val().length) return;
    reg ? register() : authenticate();
  });
  $('.toggle').click(() => {
    toggleRegAuth();
  });
});

function toggleRegAuth() {
  if (reg) {
    $('.toggle').text('Create account');
    $('.btn').text('Sign in');
    $('#err-txt').text('Incorrect user name');
  } else {
    $('.toggle').text('Have an account? Sign in');
    $('.btn').text('Register');
    $('#err-txt').text('User name not available');
  }
  $('#uname').val('');
  $('#err-txt').hide();
  reg = !reg;
}

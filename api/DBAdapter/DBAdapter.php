<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
class DBAdapter
{
    private $DB;

    public function __construct()
    {
        $dataBase =
            'mysql:dbname=yourdb;charset=latin1;host=yourhost';
        $user = 'you';
        $password = 'yourpassword';
        try {
            $this->DB = new PDO($dataBase, $user, $password);
            $this->DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Error establishing Connection';
            exit();
        }
    }

    public function insertUser($userName)
    {
        $chk = $this->DB->prepare("SELECT * from users where username = :un");
        $chk->bindParam(':un', $userName);
        $chk->execute();
        if (count($chk->fetchAll()) == 0) {
            $stmt = $this->DB->prepare(
                "INSERT INTO users (username) VALUES (:name)"
            );
            $stmt->bindParam(':name', $userName);

            $stmt->execute();
        }
    }

    public function getUID($userName)
    {
        $stmt = $this->DB->prepare(
            "SELECT uid FROM users where username = :uname"
        );
        $stmt->bindParam(':uname', $userName);
        $stmt->execute();
        return $stmt->fetch()['uid'] ?? -1;
    }

    public function storePubKey($userName, $credId, $credPubKey)
    {
        $this->insertUser($userName);
        $userId = $this->getUID($userName);

        $stmt = $this->DB->prepare(
            "INSERT INTO webauthns (uid,credId,credPubKey) VALUES (:uid,:credId,:credPubKey)"
        );
        $stmt->bindParam(':uid', $userId);
        $stmt->bindParam(':credId', $credId);
        $stmt->bindParam(':credPubKey', $credPubKey);

        return $stmt->execute();
    }

    public function getCredIds($userName)
    {
        $uid = $this->getUID($userName);
        $uid = strval($uid);
        $stmt = $this->DB->prepare(
            "SELECT credId FROM webauthns where uid = :uid"
        );
        $stmt->bindParam(':uid', $uid);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getPubKey($userName, $id)
    {
        $uid = $this->getUID($userName);
        $stmt = $this->DB->prepare(
            "SELECT credPubKey FROM webauthns where uid = :uid and credId = :id"
        );
        $stmt->bindParam(':uid', $uid);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function isUnameTaken($userName)
    {
        $uid = $this->getUID($userName);
        $stmt = $this->DB->prepare("SELECT * FROM webauthns where uid = :uid");
        $stmt->bindParam(':uid', $uid);
        $stmt->execute();
        return $stmt->fetchAll();
    }
}

?>

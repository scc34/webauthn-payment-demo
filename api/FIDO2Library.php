<?php

namespace FIDO2Library;
use FIDO2Library\Formats\ByteBuffer;
require_once './Formats/ByteBuffer.php';
require_once './Attest/AttestObject.php';
require_once './Attest/AuthenticatorData.php';
require_once './Formats/CborDecoder.php';

class FIDO2Library
{
    private $_relyingPartyName;
    private $_relyingPartyId;
    private $_relyingPartyIdHash;
    private $_nonce;

    public function __construct($relyingPartyName, $relyingPartyId)
    {
        $this->_relyingPartyName = $relyingPartyName;
        $this->_relyingPartyId = $relyingPartyId;
        $this->_relyingPartyIdHash = \hash('sha256', $relyingPartyId, true); //store the hash for later verification (reg and auth)
    }

    //build the registration object. https://developer.mozilla.org/en-US/docs/Web/API/PublicKeyCredentialCreationOptions
    //this object is what is passed to navigator.credentials.create
    //we pass info about the relying party, which algorithms are accepted, what type of attestation we expect, various other parameters,
    // and last but not least: the challenge to be cryptographically signed by the authenticator
    public function generateRegOpts($userId, $userName, $userDisplayName)
    {
        $opts = new \stdClass();
        $opts->publicKey = new \stdClass();

        $opts->publicKey->rp = new \stdClass();
        $opts->publicKey->rp->name = $this->_relyingPartyName;
        $opts->publicKey->rp->id = $this->_relyingPartyId;

        $opts->publicKey->user = new \stdClass();
        $opts->publicKey->user->id = new ByteBuffer($userId); // binary
        $opts->publicKey->user->name = $userName;
        $opts->publicKey->user->displayName = $userDisplayName;

        $opts->publicKey->challenge = $this->_generateNonce(); // binary

        $opts->publicKey->pubKeyCredParams = [];
        $temp = new \stdClass();
        $temp->type = 'public-key';
        $temp->alg = -7; // ES256
        $opts->publicKey->pubKeyCredParams[] = $temp;
        unset($temp);

        $temp = new \stdClass();
        $temp->type = 'public-key';
        $temp->alg = -257; // RS256
        $opts->publicKey->pubKeyCredParams[] = $temp;
        unset($temp);

        $opts->publicKey->timeout = 20000;

        $opts->publicKey->authenticatorSelection = new \stdClass();
        $opts->publicKey->authenticatorSelection->userVerification = 'required';

        $opts->publicKey->attestation = 'none';

        return $opts;
    }

    //process the registration using the data sent back by the authenticator as a result of navigator.credentials.create
    //the attestationObject and clientDataJSON are the data returned by the authenticator that contain all
    //the necessary info to register a new publicKey corresponding to the authenticators private key used for this credential
    public function register($attestationObject, $clientDataJSON, $nonce)
    {
        $nonce = $nonce instanceof ByteBuffer ? $nonce : new ByteBuffer($nonce); //make sure nonce is a ByteBuffer

        $clientData = json_decode($clientDataJSON);
        $clientDataHash = hash('sha256', $clientDataJSON, true);

        //make sure clientData is an object
        if (gettype($clientData) !== "object") {
            throw new \Exception('invalid client data');
        }

        //make sure clientData type exists and is 'webauthn.cretae'
        if (
            !isset($clientData->type) ||
            $clientData->type !== 'webauthn.create'
        ) {
            throw new \Exception('invalid type');
        }

        //make sure sure the challenge the authenticator sends back is the challenge that it was sent initially
        //prevents replay attacks
        if (
            !isset($clientData->challenge) ||
            ByteBuffer::fromBase64Url(
                $clientData->challenge
            )->getBinaryStr() !== $nonce->getBinaryStr()
        ) {
            throw new \Exception('invalid challenge');
        }

        //make sure the origin the authenticator sends back matches the origin of the relying party
        if (
            !isset($clientData->origin) ||
            !$this->_verifyOrigin($clientData->origin)
        ) {
            throw new \Exception('invalid origin');
        }

        //instantiate the attestation object for validation and processing
        $attestationObject = new Attest\AttestObject($attestationObject);

        //make sure the rpid hash the authenticator sends back matches what the FIDO2Library object has
        if (!$attestationObject->validateRpIdHash($this->_relyingPartyIdHash)) {
            throw new \Exception('invalid relyingPartyId hash');
        }

        //make sure that the userPresent bits are set in the authenticators response
        if (!$attestationObject->getAuthenticatorData()->isUserPresent()) {
            throw new \Exception('user not present during authentication');
        }

        //make sure that the user was verified via biometric or pin or screenlock etc.
        if (!$attestationObject->getAuthenticatorData()->isUserVerified()) {
            throw new \Exception('user not verificated during authentication');
        }

        //If all validations passed, then return the credID and the Public key to be stored in DB for subsequent authentications
        $data = new \stdClass();
        $data->credentialId = $attestationObject
            ->getAuthenticatorData()
            ->getCredId();
        $data->credentialPublicKey = $attestationObject
            ->getAuthenticatorData()
            ->getPemPubKey();
        return $data;
    }

    //build the authentication object https://developer.mozilla.org/en-US/docs/Web/API/PublicKeyCredentialRequestOptions
    //this object is what is passed to navigator.credentials.get
    //we pass info about the relying party what properties it expects from the authenticator etc.
    //last but not least: we pass the challenge to be cryptographically signed by the authenticator
    public function generateAuthOpts($credentialIds = [])
    {
        $opts = new \stdClass();

        $opts->publicKey = new \stdClass();
        $opts->publicKey->challenge = $this->_generateNonce(); // binary
        $opts->publicKey->timeout = 20000;
        $opts->publicKey->relyingPartyId = $this->_relyingPartyId;
        $opts->publicKey->userVerification = 'required'; //ensures MFA, fo authenticators that need this specified

        if (count($credentialIds) > 0) {
            $opts->publicKey->allowCredentials = [];

            foreach ($credentialIds as $id) {
                $temp = new \stdClass();
                $temp->id =
                    $id instanceof ByteBuffer ? $id : new ByteBuffer($id); // binary

                //If want to prohibit certain authenticator types remove from this array
                $temp->transports = ['usb', 'nfc', 'ble', 'internal'];

                $temp->type = 'public-key';
                $opts->publicKey->allowCredentials[] = $temp;
                unset($temp);
            }
        }

        return $opts;
    }

    //process the authentication using the data sent back by the authenticator as a result of navigator.credentials.create
    //the attestationObject, clientDataJSON, and signature are the data returned by the authenticator that contain all
    //the necessary info to authenticate using the publicKey corresponding to the authenticators private key used for this credential
    public function authenticate(
        $clientDataJSON,
        $authenticatorData,
        $signature,
        $credentialPublicKey,
        $nonce
    ) {
        $authenticatorObj = new Attest\AuthenticatorData($authenticatorData);
        $clientDataHash = hash('sha256', $clientDataJSON, true);
        $clientData = json_decode($clientDataJSON);
        $nonce = $nonce instanceof ByteBuffer ? $nonce : new ByteBuffer($nonce); //make sure nonce is a ByteBuffer

        //make sure clientData is an object
        if (gettype($clientData) !== "object") {
            throw new \Exception('invalid client data');
        }

        //make sure clientData type exists and is 'webauthn.get'
        if (!isset($clientData->type) || $clientData->type !== 'webauthn.get') {
            throw new \Exception('invalid type');
        }

        //make sure challenge sent by authenticator exists and matches what was initially sent to it by the server
        //prevents replay attacks
        if (
            !isset($clientData->challenge) ||
            ByteBuffer::fromBase64Url(
                $clientData->challenge
            )->getBinaryStr() !== $nonce->getBinaryStr()
        ) {
            throw new \Exception('invalid challenge');
        }

        //makes sure origin sent by authenticator exists and matches what was initially sent to it by the server
        if (
            !isset($clientData->origin) ||
            !$this->_verifyOrigin($clientData->origin)
        ) {
            throw new \Exception('invalid origin');
        }

        //make sure hash of rpid sent by the authenticator matches the hash of the rpid the server has
        if ($authenticatorObj->getrpIdHash() !== $this->_relyingPartyIdHash) {
            throw new \Exception('invalid relyingPartyId hash');
        }

        //make sure the userPresent bits are set in the authenticator response
        if (!$authenticatorObj->isUserPresent()) {
            throw new \Exception('user not present during authentication');
        }

        //make sure that the user was verified via biometric or pin or screenlock etc.
        if (!$authenticatorObj->isUserVerified()) {
            throw new \Exception('user not verificated during authentication');
        }

        $dataToVerify = '';
        $dataToVerify .= $authenticatorData;
        $dataToVerify .= $clientDataHash;

        //verify the public key retrieved by the server is valid
        $publicKey = openssl_pkey_get_public($credentialPublicKey);
        if ($publicKey === false) {
            throw new \Exception('public key invalid');
        }

        //check the signed data sent by the authenticator using the signature that the authenticator sent
        //and the corresponding public key retrieved by the server
        //this is the public key crypto where we can see that authentication does not rely upon a shared secret(i.e passwordless auth)
        if (
            openssl_verify(
                $dataToVerify,
                $signature,
                $publicKey,
                OPENSSL_ALGO_SHA256
            ) !== 1
        ) {
            throw new \Exception('invalid signature');
        }

        return true;
    }

    public function getNonce()
    {
        return $this->_nonce;
    }

    private function _verifyOrigin($origin)
    {
        if (
            $this->_relyingPartyId !== 'localhost' &&
            \parse_url($origin, PHP_URL_SCHEME) !== 'https'
        ) {
            return false;
        }

        $host = \parse_url($origin, PHP_URL_HOST);
        $host = \trim($host, '.');
        return \preg_match(
            '/' . \preg_quote($this->_relyingPartyId) . '$/i',
            $host
        ) === 1;
    }

    private function _generateNonce($length = 32)
    {
        if (!$this->_nonce) {
            $this->_nonce = ByteBuffer::randomBuffer($length);
        }
        return $this->_nonce;
    }
}

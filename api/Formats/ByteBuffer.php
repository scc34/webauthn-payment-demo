<?php

namespace FIDO2Library\Formats;

//modified from https://github.com/madwizard-thomas/webauthn-server/blob/master/src/Format/ByteBuffer.php
class ByteBuffer implements \JsonSerializable, \Serializable
{
    private $_length;

    public function __construct($binaryData)
    {
        $this->_data = $binaryData;
        $this->_length = \strlen($binaryData);
    }

    /**
     * create a ByteBuffer from a base64 url encoded string
     * @param string $base64url
     * @return \FIDO2Library\Formats\ByteBuffer
     */
    public static function fromBase64Url($base64url)
    {
        $bin = self::_base64url_decode($base64url);
        if ($bin === false) {
            throw new \Exception('ByteBuffer: Invalid base64 url string');
        }
        return new ByteBuffer($bin);
    }

    /**
     * create a random ByteBuffer
     * @param string $length
     * @return \FIDO2Library\Formats\ByteBuffer
     */
    public static function randomBuffer($length)
    {
        return new ByteBuffer(\random_bytes($length));
    }

    public function getBytes($offset, $length)
    {
        if ($offset < 0 || $length < 0 || $offset + $length > $this->_length) {
            throw new \Exception('ByteBuffer: Invalid offset or length');
        }
        return \substr($this->_data, $offset, $length);
    }

    public function getByteVal($pos)
    {
        if ($pos < 0 || $pos >= $this->_length) {
            throw new \Exception('ByteBuffer: Invalid offset');
        }
        return \ord(\substr($this->_data, $pos, 1));
    }

    public function getLength()
    {
        return $this->_length;
    }

    public function getUint16Val($offset)
    {
        if ($offset < 0 || $offset + 2 > $this->_length) {
            throw new \Exception('ByteBuffer: Invalid offset');
        }
        // echo("the char at offset pos: ".$this->_data[$offset]."\n");
        // echo("the unpacked result: ".unpack('n', $this->_data, $offset)[1]."\n");
        return unpack('n', $this->_data, $offset)[1];
    }

    public function getUint32Val($offset)
    {
        if ($offset < 0 || $offset + 4 > $this->_length) {
            throw new \Exception('ByteBuffer: Invalid offset');
        }
        $val = unpack('N', $this->_data, $offset)[1];

        // Signed integer overflow causes signed negative numbers
        if ($val < 0) {
            throw new \Exception('ByteBuffer: Value out of integer range.');
        }
        return $val;
    }

    public function getUint64Val($offset)
    {
        if (PHP_INT_SIZE < 8) {
            throw new \Exception(
                'ByteBuffer: 64-bit values not supported by this system'
            );
        }
        if ($offset < 0 || $offset + 8 > $this->_length) {
            throw new \Exception('ByteBuffer: Invalid offset');
        }
        $val = unpack('J', $this->_data, $offset)[1];

        // Signed integer overflow causes signed negative numbers
        if ($val < 0) {
            throw new \Exception('ByteBuffer: Value out of integer range.');
        }

        return $val;
    }

    public function getHalfFloatVal($offset)
    {
        //FROM spec pseudo decode_half(unsigned char *halfp)
        $half = $this->getUint16Val($offset);

        $exp = ($half >> 10) & 0x1f;
        $mant = $half & 0x3ff;

        if ($exp === 0) {
            $val = $mant * 2 ** -24;
        } elseif ($exp !== 31) {
            $val = ($mant + 1024) * 2 ** ($exp - 25);
        } else {
            $val = $mant === 0 ? INF : NAN;
        }

        return $half & 0x8000 ? -$val : $val;
    }

    public function getFloatVal($offset)
    {
        if ($offset < 0 || $offset + 4 > $this->_length) {
            throw new \Exception('ByteBuffer: Invalid offset');
        }
        return unpack('G', $this->_data, $offset)[1];
    }

    public function getDoubleVal($offset)
    {
        if ($offset < 0 || $offset + 8 > $this->_length) {
            throw new \Exception('ByteBuffer: Invalid offset');
        }
        return unpack('E', $this->_data, $offset)[1];
    }

    /**
     * @return string
     */
    public function getBinaryStr()
    {
        return $this->_data;
    }

    /**
     * @param string $buffer
     * @return bool
     */
    public function equals($buffer)
    {
        return is_string($this->_data) && $this->_data === $buffer->data;
    }

    /**
     * @return string
     */
    public function getHex()
    {
        return \bin2hex($this->_data);
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return $this->_length === 0;
    }

    /**
     * jsonSerialize interface
     * return binary data in RFC 1342-Like serialized string
     * @return \stdClass
     */
    public function jsonSerialize()
    {
        return '=?BINARY?B?' . \base64_encode($this->_data) . '?=';
    }

    /**
     * Serializable-Interface
     * @return string
     */
    public function serialize()
    {
        return \serialize($this->_data);
    }

    /**
     * Serializable-Interface
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $this->_data = \unserialize($serialized);
        $this->_length = \strlen($this->_data);
    }

    // -----------------------
    // PROTECTED STATIC
    // -----------------------

    /**
     * base64 url decoding
     * @param string $data
     * @return string
     */
    protected static function _base64url_decode($data)
    {
        return \base64_decode(
            \strtr($data, '-_', '+/') .
                \str_repeat('=', 3 - ((3 + \strlen($data)) % 4))
        );
    }

    /**
     * base64 url encoding
     * @param string $data
     * @return string
     */
    protected static function _base64url_encode($data)
    {
        return \rtrim(\strtr(\base64_encode($data), '+/', '-_'), '=');
    }
}

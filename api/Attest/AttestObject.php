<?php

namespace FIDO2Library\Attest;
use FIDO2Library\Formats\CborDecoder;
use FIDO2Library\Formats\ByteBuffer;

class AttestObject
{
    private $_authenticatorData;

    public function __construct($binary)
    {
        $atObj = CborDecoder::decode($binary);

        // check to make sure format field is present
        if (
            !\is_array($atObj) ||
            !\array_key_exists('fmt', $atObj) ||
            !is_string($atObj['fmt'])
        ) {
            throw new \Exception('invalid attestation format');
        }

        //check to make sure attestation statement in present
        if (
            !\array_key_exists('attStmt', $atObj) ||
            !\is_array($atObj['attStmt'])
        ) {
            throw new \Exception(
                'invalid attestation format (attStmt not available)'
            );
        }

        //check to make sure autenticator data is present
        if (
            !\array_key_exists('authData', $atObj) ||
            !\is_object($atObj['authData']) ||
            !($atObj['authData'] instanceof ByteBuffer)
        ) {
            throw new \Exception(
                'invalid attestation format (authData not available)'
            );
        }
        //instantiate AuthenticatorData object for handling credential auth and
        $this->_authenticatorData = new AuthenticatorData(
            $atObj['authData']->getBinaryStr()
        );
    }

    public function getAuthenticatorData()
    {
        return $this->_authenticatorData;
    }

    public function validateRpIdHash($rpIdHash)
    {
        return $rpIdHash === $this->_authenticatorData->getRpIdHash();
    }
}

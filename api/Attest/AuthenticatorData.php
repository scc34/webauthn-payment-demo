<?php

namespace FIDO2Library\Attest;
use FIDO2Library\Formats\CborDecoder;
use FIDO2Library\Formats\ByteBuffer;

class AuthenticatorData
{
    protected $_authenticatorFlags;
    protected $_attestedData;
    protected $_rawBinary;
    protected $_rpIdHash;

    // Cose encoded keys
    private static $_COSE_KTY = 1;
    private static $_COSE_ALG = 3;

    // Cose EC2 ES256 P-256 curve
    private static $_COSE_CRV = -1;
    private static $_COSE_X = -2;
    private static $_COSE_Y = -3;

    // Cose RSA PS256
    private static $_COSE_N = -1;
    private static $_COSE_E = -2;

    private static $_EC2_TYPE = 2;
    private static $_EC2_ES256 = -7;
    private static $_EC2_P256 = 1;

    private static $_RSA_TYPE = 3;
    private static $_RSA_RS256 = -257;

    public function __construct($rawBin)
    {
        if (!\is_string($rawBin) || \strlen($rawBin) < 37) {
            throw new \Exception('Invalid authenticatorData input');
        }

        $this->_rawBinary = $rawBin;

        $this->_rpIdHash = \substr($rawBin, 0, 32);

        // flags 8 bits
        $binFlags = \unpack('Cflags', \substr($rawBin, 32, 1))['flags'];
        $this->_authenticatorFlags = $this->_parseFlags($binFlags);

        $offset = 37;
        if ($this->_authenticatorFlags->AT) {
            $this->_attestedData = $this->_parseAttestAuthData(
                $rawBin,
                $offset
            );
        }
    }

    public function getCredId()
    {
        if (!($this->_attestedData instanceof \stdClass)) {
            throw new \Exception(
                'credential id not included in authenticator data'
            );
        }
        return $this->_attestedData->credId;
    }

    public function getU2FPubKey()
    {
        if (!($this->_attestedData instanceof \stdClass)) {
            throw new \Exception(
                'credential data not included in authenticator data'
            );
        }
        return "\x04" .
            $this->_attestedData->credPubKey->x .
            $this->_attestedData->credPubKey->y;
    }

    public function isUserVerified()
    {
        return $this->_authenticatorFlags->UV;
    }

    public function isUserPresent()
    {
        return $this->_authenticatorFlags->UP;
    }

    public function getRpIdHash()
    {
        return $this->_rpIdHash;
    }

    public function getPemPubKey()
    {
        $der = null;
        switch ($this->_attestedData->credPubKey->kty) {
            case self::$_EC2_TYPE:
                $der = $this->_get_DER_key_ec2();
                break;
            case self::$_RSA_TYPE:
                $der = $this->_get_DER_key_rsa();
                break;
            default:
                throw new \Exception('invalid key type');
        }

        $pem = '-----BEGIN PUBLIC KEY-----' . "\n";
        $pem .= \chunk_split(\base64_encode($der), 64, "\n");
        $pem .= '-----END PUBLIC KEY-----' . "\n";
        return $pem;
    }

    //return der encoded ec2 key
    private function _get_DER_key_ec2()
    {
        return $this->_DER_seq(
            $this->_DER_seq(
                $this->_DER_oid("\x2A\x86\x48\xCE\x3D\x02\x01") .
                    $this->_DER_oid("\x2A\x86\x48\xCE\x3D\x03\x01\x07")
            ) . $this->_DER_bitStr($this->getU2FPubKey())
        );
    }

    //return der encoded rsa key
    private function _get_DER_key_rsa()
    {
        return $this->_DER_seq(
            $this->_DER_seq(
                $this->_DER_oid("\x2A\x86\x48\x86\xF7\x0D\x01\x01\x01") .
                    $this->_DER_null()
            ) .
                $this->_DER_bitStr(
                    $this->_DER_seq(
                        $this->_DER_unsignedInt(
                            $this->_attestedData->credPubKey->n
                        ) .
                            $this->_DER_unsignedInt(
                                $this->_attestedData->credPubKey->e
                            )
                    )
                )
        );
    }

    //read the authenticator flags from the flag bits
    private function _parseFlags($authenticatorFlag)
    {
        $binFlags = new \stdClass();

        $binFlags->bit_0 = !!($authenticatorFlag & 1);
        $binFlags->bit_1 = !!($authenticatorFlag & 2);
        $binFlags->bit_2 = !!($authenticatorFlag & 4);
        $binFlags->bit_3 = !!($authenticatorFlag & 8);
        $binFlags->bit_4 = !!($authenticatorFlag & 16);
        $binFlags->bit_5 = !!($authenticatorFlag & 32);
        $binFlags->bit_6 = !!($authenticatorFlag & 64);
        $binFlags->bit_7 = !!($authenticatorFlag & 128);

        //user present flag
        $binFlags->UP = $binFlags->bit_0;
        //user verified flag
        $binFlags->UV = $binFlags->bit_2;
        //attestation data included flag
        $binFlags->AT = $binFlags->bit_6;
        //extension data included flag
        $binFlags->ED = $binFlags->bit_7;
        return $binFlags;
    }

    //parse attested data returned by the authenticator
    private function _parseAttestAuthData($rawBin, &$offsetEnd)
    {
        $attestCredData = new \stdClass();
        if (\strlen($rawBin) <= 55) {
            throw new \Exception(
                'Attested data should be present but is missing'
            );
        }

        //byte lenth of the cred id.
        $length = \unpack('nlength', \substr($rawBin, 53, 2))['length'];
        $attestCredData->credId = \substr($rawBin, 55, $length);

        // reset the ending offset
        $offsetEnd = 55 + $length;

        // extract the public key
        $attestCredData->credPubKey = $this->_parseCredPubKey(
            $rawBin,
            55 + $length,
            $offsetEnd
        );

        return $attestCredData;
    }

    // parse COSE key-encoded elliptic curve pubKey ec2 format
    private function _parseCredPubKey($rawBin, $offset, &$offsetEnd)
    {
        $enc = CborDecoder::decodeInPlace($rawBin, $offset, $offsetEnd);

        $credPubKey = new \stdClass();
        $credPubKey->kty = $enc[self::$_COSE_KTY];
        $credPubKey->alg = $enc[self::$_COSE_ALG];

        switch ($credPubKey->alg) {
            case self::$_EC2_ES256:
                $this->_parseCredPubKeyES256($credPubKey, $enc);
                break;
            case self::$_RSA_RS256:
                $this->_parseCredPubKeyRS256($credPubKey, $enc);
                break;
        }

        return $credPubKey;
    }

    //parse ES256 from COSE format
    private function _parseCredPubKeyES256(&$credPubKey, $enc)
    {
        $credPubKey->crv = $enc[self::$_COSE_CRV];
        $credPubKey->x =
            $enc[self::$_COSE_X] instanceof ByteBuffer
                ? $enc[self::$_COSE_X]->getBinaryStr()
                : null;
        $credPubKey->y =
            $enc[self::$_COSE_Y] instanceof ByteBuffer
                ? $enc[self::$_COSE_Y]->getBinaryStr()
                : null;
        unset($enc);

        //check if pubkey in ec2 format
        if ($credPubKey->kty !== self::$_EC2_TYPE) {
            throw new \Exception('public key not in EC2 format');
        }

        //check if algortihm is ES256
        if ($credPubKey->alg !== self::$_EC2_ES256) {
            throw new \Exception('signature algorithm not ES256');
        }

        //check if curve is P256
        if ($credPubKey->crv !== self::$_EC2_P256) {
            throw new \Exception('curve not P256');
        }

        //check coordinates for elliptic curve
        if (\strlen($credPubKey->x) !== 32) {
            throw new \Exception('Invalid X-coordinate');
        }

        if (\strlen($credPubKey->y) !== 32) {
            throw new \Exception('Invalid Y-coordinate');
        }
    }

    //parse RS256 from COSE format
    private function _parseCredPubKeyRS256(&$credPubKey, $enc)
    {
        $credPubKey->n =
            $enc[self::$_COSE_N] instanceof ByteBuffer
                ? $enc[self::$_COSE_N]->getBinaryStr()
                : null;
        $credPubKey->e =
            $enc[self::$_COSE_E] instanceof ByteBuffer
                ? $enc[self::$_COSE_E]->getBinaryStr()
                : null;
        unset($enc);

        // Check public key in RSA format
        if ($credPubKey->kty !== self::$_RSA_TYPE) {
            throw new \Exception('public key not in RSA format');
        }

        //check algorithm is RS256
        if ($credPubKey->alg !== self::$_RSA_RS256) {
            throw new \Exception('signature algorithm not RS256');
        }

        //check the RSA modulo is valid
        if (\strlen($credPubKey->n) !== 256) {
            throw new \Exception('Invalid RSA modulus');
        }

        //check for valid RSA exponent is used
        if (\strlen($credPubKey->e) !== 3) {
            throw new \Exception('Invalid RSA public exponent');
        }
    }

    // ---------------
    // helper functions for DER format
    // ---------------

    private function _DER_len($length)
    {
        if ($length < 128) {
            return \chr($length);
        }
        $lenBytes = '';
        while ($length > 0) {
            $lenBytes = \chr($length % 256) . $lenBytes;
            $length = \intdiv($length, 256);
        }
        return \chr(0x80 | \strlen($lenBytes)) . $lenBytes;
    }

    private function _DER_seq($contents)
    {
        return "\x30" . $this->_DER_len(\strlen($contents)) . $contents;
    }

    private function _DER_oid($encoded)
    {
        return "\x06" . $this->_DER_len(\strlen($encoded)) . $encoded;
    }

    private function _DER_bitStr($bytes)
    {
        return "\x03" . $this->_DER_len(\strlen($bytes) + 1) . "\x00" . $bytes;
    }

    private function _DER_null()
    {
        return "\x05\x00";
    }

    private function _DER_unsignedInt($bytes)
    {
        $length = \strlen($bytes);

        // throw out leading zero bytes
        for ($i = 0; $i < $length - 1; $i++) {
            if (\ord($bytes[$i]) !== 0) {
                break;
            }
        }
        if ($i !== 0) {
            $bytes = \substr($bytes, $i);
        }

        //prepend a zero if the first bit is zero, to avoid sign issues
        if ((\ord($bytes[0]) & 0x80) !== 0) {
            $bytes = "\x00" . $bytes;
        }

        return "\x02" . $this->_DER_len(\strlen($bytes)) . $bytes;
    }
}

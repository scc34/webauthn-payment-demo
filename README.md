# WebAuthn Payment Demo

This demo is meant to showcase the potential for WebAuthn to be used as a payment verification method
to meet the strong authentication requirement of the 3DS2.0 specification for credit card payments.

This demo contains a bare-bones FIDO2 Library to allow a relying party to register and authenticate users
via the WebAuthn specification.

Currently this demo does not support authenticator attestation. This demo currently supports using Windows Hello,
iOS mobile devices with touchID, and Android fingerprint scanner as the hardware authenticators.

A live example is currently hosted at [secure-cc34.com](https://secure-cc34.com/)
